import java.util.Scanner;

public class Change {

	public static void main(String[] args) {
		
		//declare variables and  values 
		double getPrice;
		int getChange,newChange,quarter,dime,nickel;
		int dollar = 100;
		
		
		
		System.out.print("Enter the price of the item: ");
		Scanner input = new Scanner(System.in);
		
		//invoke user input and set input equal to getPrice variable 
		getPrice = input.nextDouble();
		
		//cast integer primitive data type to double getPrice variable 
		int testPrice = (int) getPrice;
		//Used Conditions to check if the number user inputed is greater than or equal to 25, less then 100, and is divisible by 5
		//If none of these conditions are passed, the program will continue to prompt the user to enter a number
		//Provided 3 tries for User, IF user still fails to fit these conditions, the program will end  
		if((testPrice%5!=0) || testPrice>100 || (testPrice<25)){
			System.out.println("Please enter a price divisible by 5 which is either less than or equal to $1.00 and is greater than or equal to 25 cents  ");
			getPrice=input.nextDouble();
			 testPrice = (int) getPrice;
			 if((testPrice%5!=0) || testPrice>100 || (testPrice<25)){
					System.out.println("Please enter a price divisible by 5 which is either less than or equal to $1.00 and is greater than or equal to  25 cents");
					getPrice=input.nextDouble();
					 testPrice = (int) getPrice;
					 if((testPrice%5!=0) || testPrice>100 || (testPrice<25)){
						 System.out.println("Please enter a price divisible by 5 which is either less than or equal to $1.00 and is greater than or equal to 25 cents");
						 getPrice=input.nextDouble();
						 testPrice = (int) getPrice;
						 if((testPrice%5!=0) || testPrice>100 || (testPrice<25)){
							 System.out.println("Sorry you have used your maximum tries: Program exiting");
							 System.exit(0);
						 }
						 else{
							 getPrice = input.nextDouble();
							 testPrice = (int) getPrice;
						 }
					 }
				}
			 
			 
			
		}
		
		
		
		input.close();
		
		//Calculate change in quarters, dime, and nickels 
		getChange = (int) (dollar - getPrice);
		quarter = getChange/25;
		newChange = getChange%25;
		getChange = newChange;
		dime =  (getChange/10);
		newChange=getChange%10;
		getChange = newChange;
		nickel = (getChange/5);
		newChange=getChange%5;
		
		int initialPrice = (int) getPrice;
		//Print remaining change through the number of quarters, nickels, and dimes,
		System.out.println("You bought an item for "+ initialPrice+ " cents and gave me a dollar, so your change is:");
		System.out.println(quarter + " quarters");
		System.out.println(dime+ " dimes, and");
		System.out.println(nickel + " nickels");
		
	}

}
